var express = require('express');
var app = express();
var path = require('path');
var passport = require('passport');
var session = require('express-session');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars')
var env = require('dotenv').load();

//For BodyParser
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//For Passport
app.use(session({secret : 'keyboard cat', resave : true, saveUninitialized : true}))
app.use(passport.initialize());
app.use(passport.session());

//For Handlebars
app.set('views', './app/views')
app.engine('hbs', exphbs({
    extname: '.hbs'
}));
app.set('view engine', 'ejs');

// app.get('/', function(req, res){
//     res.send('Welcome to passport with Sequaelize')
// });

//model
var models = require("./app/models"); 

//Routes
// var authRoute = require('./app/routes/auth.js');
var items = require('./app/routes/item.js');

//load passport strategies
require('./app/config/passport/passport.js')(passport, models.user);

//Sync Database
models.sequelize.sync().then(function() {
    console.log('Nice! Database looks fine')
}).catch(function(err) {
   console.log(err, "Something went wrong with the Database Update!")
});
var authroutes = require('./app/routes/auth.js');
app.use(express.static(path.join(__dirname, 'public')));

app.use('/items', items);
app.use('/', authroutes);

app.listen(3000, function(err){
    if (!err)
    {
        console.log("Site is live");
    }
    else{
        console.log(err);
    }
});
const Login = function () {
        const handleAddForm = () => {
            
                $("#add_form").validate({
                errorElement: 'span', //default input error message container
                errorClass: 'error-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        required: true
                    },
                    description: {
                        required: true
                    },
                    photo: {
                        required: true,
                    }
                },
                messages: {
                    name : {
                    required : 'Please enter MTN account number'
                    },
                    description : {
                    required : 'Please enter MTN account name',
                    },
                    photo: {
                        required: "required",
                    }
                }
            });

        }
        const handleEditForm = () => {
            $("#edit_form").validate({
                errorElement: 'span', //default input error message container
                errorClass: 'error-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        required: true
                    },
                    description: {
                        required: true
                    },
                },
                messages: {
                    name : {
                    required : 'Please enter MTN account number'
                    },
                    description : {
                    required : 'Please enter MTN account name',
                    },
                }
            });
        }
        return {
            //main function to initiate the module
            init: function () {
                handleAddForm();
                handleEditForm();
            }
        };
    }();
    
    jQuery(document).ready(function () {
        Login.init();
    });

    $(document).on("click", "#view_btn", function () {
        var itemid = $(this).data('itemid');
        url = "http://localhost:5000/items/get_item/"+itemid;

        fetch(url,{
            method: "GET",
            headers: {
              "Content-Type": "application/json"
            }}).then(function(response) {
            var json = response.json();
            var promise1 = Promise.resolve(json);
            promise1.then(function(value) {
                $("#item_name").html(value.items.name);
                $("#item_description").html(value.items.description);
            });  
          }, function(error) {
            // handle network error
          })
    });
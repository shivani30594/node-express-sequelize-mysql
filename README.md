#CRUD using Node+Express+Mysql

Illustrate the CRUD performance using Express as framework, Node as back-end and MySQL as a database. 

#What is Nodejs?

Node.js� is a JavaScript runtime built on Chrome's V8 JavaScript engine.

#What is Express Framework?

Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications. 

### Installation
npm install or npm update

### Other Dependency
Create database locally and change the database configuration in app/config/config.json

### Command
node server.js  OR nodemon server.js

### Running Locally on localhost:3000/



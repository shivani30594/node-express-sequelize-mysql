'use strict';
const _ = require('lodash');
const multer = require('multer');
var fs = require('fs');
const Item = require('./../models').item;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, _.replace(file.originalname, ' ', '_'))
    }
  })

exports.list = function(req, res) {
    Item
    .all()
    .then((items)=> {
        res.render('items/index', {data: items,page_name:'list'});
    })
    .catch(error => res.status(400).send(error));
};

exports.add = function(req, res)
{
    res.render("items/add", {page_name:'list'})
}

var upload = multer({ storage: storage }).single('photo')
exports.create = function(req, res)
{
    upload(req, res, function (err) {
        if (err) {
          res.send(err)
            console.log(err)
        }
        else{
            const item = {
                name: req.body.name,
                description: req.body.description,
                photo : _.replace(req.file.originalname, ' ', '_')
            }
            Item
            .create(item)
            .then((items)=> {
                res.redirect("list");
            })
        }
    });
}

exports.get_item = function(req, res)
{
    Item.findById(req.params.id)
    .then((items) => {
        res.send({ success: true, message: 'Failed to authenticate token.', items: items });
    }).catch((err) => {
        res.render('error', err);
    })
}
exports.edit =  function(req, res) {
    Item.findById(req.params.id)
		.then((items) => {
			res.render('items/edit', {data: items, path : 'https://cloud.githubusercontent.com/assets/9121022/12010937/', page_name:'list'})
		}).catch((err) => {
			res.render('error', err);
		})
}

exports.update =  function(req, res)
{
    upload(req, res, function (err) {
        const item = {
            name: req.body.name,
            description: req.body.description,
        }
        if (req.file)
        {
            if (req.body.old_photo !== '')
			    fs.unlink(`public/uploads/${req.body.old_photo}`);
		        item.photo = _.replace(req.file.originalname, ' ', '_')
        }
        if(err)
        {
            return err;
        }
        else{
            Item.update(item, {
                where: {
                    id: req.params.id
                }
            }).then((item) => {
                res.redirect('/list')
            }).catch((err) => {
                res.render('error', err);
            })
        }
    });
 
}

exports.delete =  function(req, res) {
    Item.findById(req.params.id)
		.then((item) => {
			fs.unlink(`public/uploads/${item.photo}`, () => {
                Item.destroy({
                where: {
                    id: item.id
                }
            }).then(() => {
                res.redirect('/list')
            }).catch((err) => {
                res.render('error', err)
            })
        });
		}).catch((err) => {
			res.render('error', err);
		})
}
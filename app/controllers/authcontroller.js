var exports = module.exports = {}
 
exports.signup = function(req, res) {
     res.render('signup');
}
exports.signin = function(req, res) {
    res.render('signin');
}
exports.dashboard = function(req, res) {
    res.render('dashboard',{username: req.user.firstname + req.user.lastname, page_name: "dashboard"});
}
exports.logout = function(req, res) {
    req.session.destroy(function(err){
        res.redirect('/');
    });
}
var express = require('express');
var router = express.Router();
var passport = require('passport');

var authController = require('../controllers/authcontroller.js');
var itemController = require('../controllers/itemcontroller.js');

// signin router
router.get('/', authController.signin);
router.post('/signin', passport.authenticate('local-signin', {
    successRedirect: '/dashboard',
    failureRedirect: '/'
    }
));

// signup router
router.get('/signup', authController.signup);
router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/dashboard',
    failureRedirect: '/signup'
    }
));

//dashboard 
router.get('/dashboard',isLoggedIn, authController.dashboard);
router.get('/logout',authController.logout);

// item listing router
router.post('/edit/:id', itemController.update);
router.get('/list', itemController.list ); 
router.post('/item', itemController.create); 
router.get('/edit/:id', itemController.edit);
router.get('/delete/:id', itemController.delete);
router.get('/add', itemController.add);

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/signin');
    }

module.exports = router;

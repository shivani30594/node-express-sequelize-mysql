var express = require('express');
var router = express.Router();
var itemController = require('../controllers/itemcontroller.js');

//get
router.get('/add', function (req, res, next) {
    res.render('items/add');
});
router.get('/list', itemController.list ); 
router.get('/edit/:id', itemController.edit);
router.get('/delete/:id', itemController.delete);
router.get('/get_item/:id', itemController.get_item);
//post
router.post('/edit/:id', itemController.update);
router.post('/item', itemController.create); 

module.exports = router;